# Steven Nickson. AEM 1303 Team A. Chris Reagan. 3/14/2021.
# NOTE: Firewall included in calculations

# This file is used to perform analysis on a given flight design.
import sympy

# We alias `sympy.sympify` to `sym` to make the code more readable. This is
# necessary because Python floats are not precise, and so Sympy floats must
# be created using strings.
sym = sympy.sympify

# ===== Input symbols =====
# Main wing properties
b = sympy.Symbol("b", real=True, positive=True) # Span
c = sympy.Symbol("c", real=True, positive=True) # Chord
tc = sympy.Symbol("tc", real=True, positive=True) # Thickness
C_l_0 = sympy.Symbol("C_l_0", real=True, positive=True) # C_l at alpha = 0
C_l_alpha = sympy.Symbol("C_l_α", real=True, positive=True)
alpha_max = sympy.Symbol("α_max", real=True, positive=True)

# Vertical tail properties
b_V = sympy.Symbol("b_V", real=True, positive=True) # Span
c_V = sympy.Symbol("c_V", real=True, positive=True) # Chord
tc_V = sympy.Symbol("tc_V", real=True, positive=True) # Thickness
C_l_0_V = sympy.Symbol("C_l_0_V", real=True, positive=True) # C_l at alpha = 0
C_l_alpha_V = sympy.Symbol("C_l_α_V", real=True, positive=True)
alpha_max_V = sympy.Symbol("α_max_V", real=True, positive=True)

# Horizontal tail properties
b_H = sympy.Symbol("b_H", real=True, positive=True) # Span
c_H = sympy.Symbol("c_H", real=True, positive=True) # Chord
tc_H = sympy.Symbol("tc_H", real=True, positive=True) # Thickness
C_l_0_H = sympy.Symbol("C_l_0_H", real=True, positive=True) # C_l at alpha = 0
C_l_alpha_H = sympy.Symbol("C_l_α_H", real=True, positive=True)
alpha_max_H = sympy.Symbol("α_max_H", real=True, positive=True)

# Propeller properties
propDiameter = sympy.Symbol("propPitch", real=True, positive=True)
propPitch = sympy.Symbol("propDiameter", real=True, positive=True)
T_static = sympy.Symbol("T_static", real=True, positive=True)
propRateMax = sympy.Symbol("propRateMax", real=True, positive=True)

# Aircraft properties
m = sympy.Symbol("m", real=True, positive=True) # Mass
epsilon = sympy.Symbol("ε", real=True, positive=True) # Oswald
k_form = sympy.Symbol("k_form", real=True, positive=True)
k_Intfr = sympy.Symbol("k_Intfr", real=True, positive=True)
fus_length = sympy.Symbol("fus_length", real=True, positive=True)
fus_width = sympy.Symbol("fus_width", real=True, positive=True)
fus_height = sympy.Symbol("fus_height", real=True, positive=True)

# Enviroment properties
v = sympy.Symbol("v", real=True, positive=True) # Velocity, m/s
g = sympy.Symbol("g", real=True, positive=True) # Gravitational acceleration
rho = sympy.Symbol("ρ", real=True, positive=True)
nu = sympy.Symbol("ν", real=True, positive=True)


# ===== Basic calculations =====
S_ref = b * c
S_wet = 2 * S_ref * (1 + sym(".2") * tc)
AR = b**2 / S_ref

S_V = b_V * c_V
S_wet_V = 2 * S_V * (1 + sym("0.2") + tc_V)

S_H = b_H * c_H
S_wet_H = 2 * S_H * (1 + sym("0.2") + tc_H)

S_fus = 2 * (fus_width + fus_height) * fus_length

W = m * g

q = sym("0.5") * rho * v**2
k_aero = 1 / (sympy.pi * AR * epsilon)

# ===== Lift coefficent =====
C_L_alpha = C_l_alpha / (1 + ((C_l_alpha) / (sympy.pi * AR * epsilon)))
# Since C_L_alpha is the slope of a linear curve which is a function of alpha,
# we have
C_L_0 = C_l_0 / (1 + ((C_l_0) / (sympy.pi * AR * epsilon)))
# Point-slope form yields a function C_L(alpha), and we have
C_L_max = C_L_alpha * alpha_max + C_L_0
# We also have the C_L for steady-level flight:
C_L = W / (sym("0.5") * rho * v**2 * S_ref)
# And the associated angle of attack
alpha = (C_L - C_L_0) / C_L_alpha


# ===== Stall speed =====
v_stall = sympy.sqrt((2 * W) / (rho * S_ref * C_L_max));


# ===== 2G bank =====
phi = 60 * (sympy.pi / 180) # 60 degree bank angle yields a 2G turn
n_turn_60 = 1 / sympy.cos(phi) # Load factor
v_stall_60 = n_turn_60**sym("0.5") * v_stall


# ===== Turning radius =====
# For the turn radius of a 60 degree bank, we have
R_60 = (v_stall_60**2) / (g * sympy.tan(phi))


# ===== Drag =====
C_D_i = C_L**2 / (sympy.pi * AR * epsilon)

Re_wing = (v * c) / nu
C_f_wing = sym("0.455") * sympy.log(Re_wing, 10)**sym("-2.58")
C_D_frict_wing = C_f_wing * (S_wet / S_ref)

Re_V = (v * c_V) / nu
C_f_V = sym("0.455") * sympy.log(Re_V, 10)**sym("-2.58")
C_D_frict_V = C_f_V * (S_wet_V / S_ref)

Re_H = (v * c_H) / nu
C_f_H = sym("0.455") * sympy.log(Re_H, 10)**sym("-2.58")
C_D_frict_H = C_f_H * (S_wet_H / S_ref)

Re_fus = (v * fus_length) / nu
C_f_fus = sym("0.455") * sympy.log(Re_fus, 10)**sym("-2.58")
C_D_frict_fus = S_fus / S_ref * C_f_fus

# Total skin friction drag:
C_D_frict = C_D_frict_wing + C_D_frict_V + C_D_frict_H + C_D_frict_fus

''' Homework model (apparently inconsistent with the prototype code):
C_D_firewall = sym("1.2") * fus_width * fus_height
# C_D_firewall = sym("1.2") * (sym("0.0058") / S_ref)
# C_D_form = k_form * C_D_frict + C_D_firewall
C_D_profile = C_D_form + C_D_frict
C_D_Intfr = k_Intfr * C_D_frict
C_D_0 = C_D_frict + C_D_form + C_D_Intfr + C_D_profile
'''
# Prototype model for parasitic drag:
C_D_profile = k_form * C_D_frict
C_D_Intfr = k_Intfr * C_D_frict
C_D_firewall = sym("1.2") * (fus_width * fus_height) / S_ref
C_D_0 = C_D_frict + C_D_profile + C_D_Intfr + C_D_firewall

C_D = C_D_0 + C_D_i

D_i = sym("0.5") * rho * v**2 * S_ref * C_D_i
D_0 = sym("0.5") * rho * v**2 * S_ref * C_D_0
D = sym("0.5") * rho * v**2 * S_ref * C_D


# ===== Lift/drag ratio =====
LD = C_L / C_D


# ===== Thrust and Propulsion =====
propAR = propPitch / propDiameter
propPitchSpeed = propRateMax * propPitch

T_dyn_a = T_static * (1.0 - v / (propPitchSpeed * (propAR + 0.2) / propAR));
T_dyn_b = T_static * (1.0 - ((v / propPitchSpeed * propAR) - (propAR - 0.6))
		/ 0.8)

T_dyn_u = sympy.Piecewise((T_dyn_a, propAR < 0.6), (T_dyn_b, propAR >= 0.6))

T_dyn = sympy.Piecewise((T_dyn_u, T_dyn_u <= T_static),
		(T_static, T_dyn_u > T_static))

T_dyn = sympy.Piecewise((T_static, T_dyn_u > T_static), (T_dyn_u, True))
P_req = v * D

RoC = (v * (T_dyn - D))/(W)
RoC_75 = (v * (sym("0.75") * T_dyn - D))/(W)
RoC_50 = (v * (sym("0.50") * T_dyn - D))/(W)
RoC_25 = (v * (sym("0.25") * T_dyn - D))/(W)

T_req_1 = D + W / v
T_req_2 = D + W * 2 / v
T_req_3 = D + W * 3 / v
T_req_4 = D + W * 4 / v

# ===== Flight envelope =====
# Structure limited load factor
n_struct = sym("3")
# Stall limited load factor
n_stall = (v / v_stall)**2
# Thrust limited load factor
n_thrust = sympy.sqrt((T_dyn - D_0) * (q * S_ref / k_aero)) / W
