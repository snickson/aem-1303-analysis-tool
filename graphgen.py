import sympy
import re
import numpy as np
import matplotlib.pyplot as plt
from derivations import *
from scipy import optimize

proto = {
		b: sym("1.2192"),
		c: sym("0.254"),
		tc: sym("0.12"),
		C_l_0: sym("0.2"),
		C_l_alpha: 2 * sympy.pi,
		alpha_max: 12 * (sympy.pi / 180),

		b_H: sym("0.4572"),
		c_H: sym("0.1778"),
		tc_H: sym("0.1"),
		C_l_0_H: sym("0"),
		C_l_alpha_H: 2 * sympy.pi,
		alpha_max_H: 14 * (sympy.pi / 180),

		b_V: sym("0.127"),
		c_V: sym("0.1778"),
		tc_V: sym("0.1"),
		C_l_0_V: sym("0"),
		C_l_alpha_V: 2 * sympy.pi,
		alpha_max_V: 14 * (sympy.pi / 180),

		m: sym("1.35"),
		epsilon: sym("0.8"),
		k_form: sym("0.30"),
		k_Intfr: sym("0.10"),
		fus_length: sym("0.508"),
		fus_width: sym("0.0762"),
		fus_height: sym("0.0762"),

		propDiameter: sym("0.2794"),
		propPitch: sym("0.1778"),
		T_static: sym("9.7015713"),
		propRateMax: sym("7576 / 60"),

		# v: sym("17"),
		g: sym("9.807"),
		rho: sym("1.185"),
		nu: sym("1.381 * 10**-5")
}

craft = {
		b: sym("1.83"),
		c: sym("0.2032"),
		tc: sym("0.12"),
		C_l_0: sym("0.2"),
		C_l_alpha: 2 * sympy.pi,
		alpha_max: 12 * (sympy.pi / 180),
		
		b_H: sym("0.4572"),
		b_H: sym("0.3"),
		c_H: sym("0.1778"),
		tc_H: sym("0.1"),
		C_l_0_H: sym("0"),
		C_l_alpha_H: 2 * sympy.pi,
		alpha_max_H: 14 * (sympy.pi / 180),

		b_V: sym("0.127"),
		c_V: sym("0.1778"),
		tc_V: sym("0.1"),
		C_l_0_V: sym("0"),
		C_l_alpha_V: 2 * sympy.pi,
		alpha_max_V: 14 * (sympy.pi / 180),

		m: sym("1.35"),
		epsilon: sym("0.8"),
		k_form: sym("0.25"),
		k_Intfr: sym("0.05"),
		fus_length: sym("0.508"),
		fus_width: sym("0.0762"),
		fus_height: sym("0.0762"),

		propDiameter: sym("0.2794"),
		propPitch: sym("0.1778"),
		T_static: sym("9.7015713"),
		propRateMax: sym("7576 / 60"),

		# v: sym("v"),
		g: sym("9.807"),
		rho: sym("1.185"),
		nu: sym("1.381 * 10**-5")
}

# Finds the maximum LD point
def max_ld_point(craftdict):
	assert v not in craftdict.keys()

	vmin = v_stall.subs(craftdict).evalf(30)
	vmax = 20 # TODO make this the thrust value
	vmid = (vmin + vmax) / 2
	ldfunction = sympy.lambdify(v, -1 * LD.subs(craftdict))
	constraint = optimize.NonlinearConstraint(lambda x: x, vmin, vmax)

	vx = optimize.minimize(ldfunction, constraints=constraint, x0=vmid).x[0]
	LDx = LD.subs(craftdict).subs(v, vx).evalf(30)
	return vx, LDx

# Finds the minimum drag point
def min_drag_point(craftdict):
	assert v not in craftdict.keys()

	vmin = v_stall.subs(craftdict)
	vmax = 20 # TODO make this the thrust value
	vmid = (vmin + vmax) / 2
	dragfunction = sympy.lambdify(v, 1 * D.subs(craftdict))
	constraint = optimize.NonlinearConstraint(lambda x: x, vmin, vmax)

	vx = optimize.minimize(dragfunction, constraints=constraint, x0=vmid).x[0]
	Dx = D.subs(craftdict).subs(v, vx).evalf(30)
	return vx, Dx

def vopt_min_distance(x):
	craftdict = {**craft, **{b: x[0]}}
	assert v not in craftdict.keys()

	vmin = v_stall.subs(craftdict)
	return max_ld_point(craftdict)[0] - vmin

# Helper function that prints out some details of the aircraft
def printcraft_approx(xcraft):
	print("b:", b.subs(xcraft).evalf(5))
	print("Re:", Re_wing.subs(xcraft).evalf(5))
	print("C_L:", C_L.subs(xcraft).evalf(5))
	print("C_D_i:", C_D_i.subs(xcraft).evalf(5))
	print("C_D_0:", C_D_0.subs(xcraft).evalf(5))
	print("D_0:", D_0.subs(xcraft).evalf(5))
	print("D_i:", D_i.subs(xcraft).evalf(5))
	print("D:", D.subs(xcraft).evalf(5))
	print("L/D:", LD.subs(xcraft).evalf(5))
	print("v_stall", v_stall.subs(xcraft).evalf(5))

# This function generates a C snippet describing the given expression.
# Can be used to export an expression to gnuplot.
def gen_cexpression(name, expression):
	return re.sub(r"\s+", "", sympy.ccode(expression))

# Draws a graph of the given expression and substitution values.
def graph_draw(title, d, r, xcraft, *expressions):
	plt.figure()

	plt.title(title)
	plt.xlabel(d[2])
	plt.ylabel(r[2])

	for exprset in expressions:
		expr = exprset[0]
		label = exprset[1]
		
		tempcraft = xcraft.copy()
		variables = list(expr.subs(tempcraft).atoms(sympy.Symbol))

		if len(variables) == 1:
			variable = variables[0]

			if variable in xcraft:
				xcraft.pop(variable)

			domain = np.linspace(d[0], d[1])
			transformation = sympy.lambdify(variable, expr.subs(xcraft), "numpy")
			erange = transformation(domain)
			plt.xlim(d[0], d[1])
			plt.ylim(r[0], r[1])

			plt.plot(domain, erange, label=label)
			plt.legend()

		elif len(variables) == 0:
			domain = np.linspace(d[0], d[1])
			erange = np.full(len(domain), expr.evalf(50))
			plt.plot(domain, erange)

		else:
			print(f"Warning: {expr} has too many variables, skipping)")
			pass


# If needed, this section can be used to optimimze for some contraints.
'''
# Get the max L/D point of the prototype aircraft
vx, ldx = max_ld_point(proto)
print("LD_max_proto:", ldx)

# Assign constraints. In this case, the "optimization" is actually root
# finding. The first constraint restricts the search space from 0.5 to 4,
# which helps prevent domain errors.
# The 2nd constraint restricts the search space to spans that results
# in a max L/D 1.5 times the protypes max L/D. There is only one solution
# to this.
constraint = (
		optimize.NonlinearConstraint(lambda x: x, 0.5, 4),
		optimize.NonlinearConstraint(
			lambda x: (max_ld_point({**craft, **{b: x}})[1]),
				1.5 * ldx, 1.5 * ldx)
)

# Perform the optimization. We minimize the linear function f(x) = x (it does
# not matter what function we choose to minimize in the case, since there is
# only one solution to the above constraints)
craft[b] = optimize.minimize(
		lambda x: x, constraints=constraint, x0=1.3).x[0]
# print("bopt:", craft[b])
vopt, ldopt = max_ld_point(craft)
print("vopt:", vopt)
print("ldopt:", ldopt)
'''

# Make the plots less ugly
plt.style.use("ggplot")

# Flight envelope
graph_draw("Flight envelope",
		(0, 20, "Velocity (m/s)"),
		(0, 5, "(nd)"),
		craft,
		(n_thrust, "Thrust factor"),
		(n_stall, "Stall factor"),
		(n_struct, "Structure factor"))

# Drag graph
graph_draw("Drag",
		(0, 20, "Velocity (m/s)"),
		(0, 5, "Drag (N)"),
		craft,
		(D, "Total drag"),
		(D_i, "Induced drag"),
		(D_0, "Parasitic drag"))

# LD graph
graph_draw("L/D ratio",
		(0, 20, "Velocity (m/s)"),
		(0, 15, "L/D ratio"),
		craft,
		(LD, "L/D ratio"))

# Dynamic thrust and drag
graph_draw("Dynamic Thrust and Drag",
		(0, 25, "Velocity (m/s)"),
		(0, 10, "Force (N)"),
		craft,
		(D, "Drag"),
		(T_dyn, "Thrust"))

# Power required vs veloctiy
graph_draw("Power Required",
		(0, 15, "Velocity (m/s)"),
		(0, 20, "Power"),
		craft,
		(P_req, "Power required"))

# Rate of clumb
graph_draw("Rate of Climb",
		(5, 20, "Velocity (m/s)"),
		(-5, 5, "Rate of Climb (m/s)"),
		craft,
		(RoC, "100% throttle"),
		(RoC_75, "75% throttle"),
		(RoC_50, "50% throttle"),
		(RoC_25, "25% throttle"))

# Thrust required
graph_draw("Thrust Required",
		(0, 20, "Velocity (m/s)"),
		(0, 15, "Thrust (N)"),
		craft,
		(T_req_1, "Thrust required for 1 m/s"),
		(T_req_2, "Thrust required for 2 m/s"),
		(T_req_3, "Thrust required for 3 m/s"),
		(T_req_4, "Thrust required for 4 m/s"))

plt.show()
